import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import Timeline from "react-calendar-timeline";

import { Layout, Icon } from "antd";

const { Header, Content } = Layout;

class TimelineRoute extends React.Component {
  render() {
    const { shifts, employees } = this.props;

    if (shifts.isLoaded) {
      const keys = Object.keys(shifts.groupByDate);

      const start = moment(keys[0]);
      const end = moment(keys[0])
        .startOf("day")
        .add(7, "day");

      return (
        <>
          <Header style={{ background: "#fff", padding: `0 24px` }}>
            Timeline
          </Header>
          <Content>
            {shifts.shiftsList ? (
              <Timeline
                groups={employees}
                items={shifts.shiftsList}
                defaultTimeStart={start}
                defaultTimeEnd={end}
                canMove={false}
                canResize={false}
                showCursorLine
              />
            ) : (
              <Icon type="loading" />
            )}
          </Content>
        </>
      );
    } else {
      return (
        <>
          <Header style={{ background: "#fff", padding: `0 24px` }}>
            Timeline
          </Header>
          <Content style={{ padding: 24, background: "#fff" }}>
            <Icon type="loading" />
          </Content>
        </>
      );
    }
  }
}

const mapStateToProps = state => ({ ...state });

export default connect(
  mapStateToProps,
  null
)(TimelineRoute);
