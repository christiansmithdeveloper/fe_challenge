import React from "react";
import { connect } from "react-redux";
import moment from "moment-timezone";
import * as actions from "../actions";

export default function routesHoc(WrappedComponent) {
  class Wrapper extends React.Component {
    componentWillMount() {
      const { fetchConfig, fetchRoles } = this.props;

      fetchConfig();
      fetchRoles();
    }

    componentDidMount() {
      const { fetchEmployees, config } = this.props;
      fetchEmployees();

      moment.tz.setDefault(config.timezone);
    }

    componentDidUpdate(nextProps, nextState) {
      const { config } = this.props;
      if (this.props.roles.length !== nextProps.roles.length) {
        nextProps.fetchShifts(this.props.roles, config.timezone);
      }
      const { shiftsList, isUpdated } = this.props.shifts;
      if (isUpdated !== nextProps.shifts.isUpdated) {
        nextProps.updateGroupShifts(shiftsList, config.timezone);
      }
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => ({ ...state });

  const mapDispatchToProps = { ...actions };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(Wrapper);
}
