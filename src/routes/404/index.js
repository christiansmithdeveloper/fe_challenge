import React from "react";

import { Layout, Typography } from "antd";
const { Header, Content } = Layout;

export default () => (
  <>
    <Header style={{ background: "#fff", padding: `0 24px` }}>
      404 Page Not Found
    </Header>
    <Content style={{ padding: 24, background: "#fff" }}>
      <Typography.Title level={4}>
        Sorry the your looking for currently not available.
      </Typography.Title>
    </Content>
  </>
);
