import React from "react";
import { connect } from "react-redux";
import * as _ from "lodash";
import moment from "moment";

import {
  Button,
  Divider,
  Badge,
  Card,
  Icon,
  Layout,
  Tag,
  Table,
  Timeline,
  Typography,
  Radio,
  Row,
  Col
} from "antd";

import ModifyShiftDrawer from "../../container/modifyEmployeeShift";

const { Header, Content } = Layout;
const { Meta } = Card;

class FeedRoute extends React.Component {
  format = "h:mm A z";

  state = {
    data: {},
    editDrawer: false,
    view: "table"
  };

  componentDidMount() {
    moment.tz.setDefault(this.props.config.timezone);
  }

  changeView = v => {
    this.setState({ view: v.target.value });
  };

  showDrawer = data => {
    this.setState({
      data: data,
      editDrawer: true
    });
  };

  onClose = () => {
    this.setState({
      data: {},
      editDrawer: false
    });
  };

  renderDatedShiftsTable = () => {
    const { employees, roles, config } = this.props;
    const { groupByDate } = this.props.shifts;

    const group = _.map(groupByDate, (v, i) => {
      return {
        label: moment.tz(i, config.timezone),
        group: v
      };
    });

    return _.map(group, (shift, i) => {
      const preparedEmployeeShifts = _.map(shift.group, shift => {
        const name = employees.find(ref => ref.id === shift.employee_id);

        const role = roles.find(ref => ref.id === shift.role_id);

        const duration = moment({})
          .startOf("day")
          .seconds(shift.break_duration)
          .format("h:mm ");

        return {
          key: shift.id,
          name: { role, name },
          start: moment
            .tz(shift.start_time, config.timezone)
            .format(this.format),
          end: moment
            .tz(shift.end_time, config.timezone)
            .format(this.format),
          break: `${duration} Hour`,
          role: role
        };
      });

      const tableConfig = [
        {
          title: `Employee Name`,
          dataIndex: "name",
          key: "name",
          render: ({ role, name }) => (
            <Typography.Text>
              <Badge color={role.background_colour} />
              {`${name.last_name}, ${name.first_name}`}
            </Typography.Text>
          )
        },
        {
          title: `Role`,
          dataIndex: "role",
          key: "role",
          render: role => (
            <Tag
              color={role.background_colour}
              style={{ color: role.text_colour }}
            >
              {role.name}
            </Tag>
          )
        },
        {
          title: `Start Time`,
          dataIndex: "start",
          key: "start"
        },
        {
          title: `End Time`,
          dataIndex: "end",
          key: "end"
        },
        {
          title: `Break Length`,
          dataIndex: "break",
          key: "break"
        },
        {
          title: ``,
          dataIndex: `key`,
          key: `key`,
          render: id => (
            <Button
              shape="round"
              icon="edit"
              onClick={() => this.showDrawer(id)}
            >
              Edit
            </Button>
          )
        }
      ];

      return (
        <Content style={{ minHeight: "unset" }} key={i}>
          <Typography.Title level={4}>
            <Divider orientation="left">
              Employee shifts for {shift.label.format("ll")}
            </Divider>
          </Typography.Title>
          <Table
            columns={tableConfig}
            dataSource={preparedEmployeeShifts}
            key={i}
          />
        </Content>
      );
    });
  };

  renderDatedShifts = () => {
    const { employees, roles, config } = this.props;
    const { groupByDate } = this.props.shifts;

    const group = _.map(groupByDate, (v, i) => {
      return {
        label: moment.tz(i, config.timezone),
        group: v
      };
    });

    return _.map(group, (shift, i) => {
      return (
        <Card
          key={i}
          title={`${shift.label.format("ll")}`}
          style={{ marginBottom: 32 }}
        >
          {_.map(shift.group, employee => {
            const { first_name, last_name } = employees.find(
              ref => ref.id === employee.employee_id
            );

            const { name, background_colour, text_colour } = roles.find(
              ref => ref.id === employee.role_id
            );

            return (
              <Card.Grid key={employee.id}>
                <Card type="inner">
                  <Content style={{ minHeight: "unset", padding: 16 }}>
                    <Meta
                      title={
                        <Typography.Paragraph style={{ marginBottom: 0 }}>
                          <Badge color={background_colour} />
                          {`${last_name}, ${first_name}`}
                        </Typography.Paragraph>
                      }
                      decription={employee.role_id}
                    />
                    <Divider orientation="left">
                      <Tag
                        color={background_colour}
                        style={{ color: text_colour }}
                      >
                        {name}
                      </Tag>
                    </Divider>
                    <Timeline>
                      <Timeline.Item
                        color="green"
                        dot={
                          <Icon
                            type="clock-circle-o"
                            style={{ fontSize: "16px" }}
                          />
                        }
                      >
                        Time starts at{" "}
                        {moment.tz(employee.start_time, config.timezone).format(this.format)}
                      </Timeline.Item>
                      <Timeline.Item>
                        Break Duration{" "}
                        {moment({})
                          .startOf("day")
                          .seconds(employee.break_duration)
                          .format("h:mm")}{" "}
                        Hour
                      </Timeline.Item>
                      <Timeline.Item
                        color="red"
                        dot={
                          <Icon
                            type="clock-circle-o"
                            style={{ fontSize: "16px" }}
                          />
                        }
                      >
                        Time ends at{" "}
                        {moment.tz(employee.end_time, config.timezone).format(this.format)}
                      </Timeline.Item>
                    </Timeline>
                  </Content>
                </Card>
              </Card.Grid>
            );
          })}
        </Card>
      );
    });
  };

  render() {
    const { shifts } = this.props;
    const { editDrawer, view, data } = this.state;

    if (shifts.isLoaded) {
      return (
        <>
          {editDrawer ? (
            <ModifyShiftDrawer
              isOpen={editDrawer}
              closeFunc={this.onClose}
              data={data}
            />
          ) : null}

          <Header style={{ background: "#fff", padding: `0 24px` }}>
            <Row align="middle" justify="end" type="flex">
              <Col span={20}>Feed</Col>
              <Col
                span={4}
                style={{ display: "flex", justifyContent: "flex-end" }}
              >
                <Radio.Group onChange={this.changeView} defaultValue="table">
                  <Radio.Button value="table">
                    <Icon type="table" />
                  </Radio.Button>
                  <Radio.Button value="card">
                    <Icon type="appstore" />
                  </Radio.Button>
                </Radio.Group>
              </Col>
            </Row>
          </Header>
          <Content style={{ padding: 24, background: "#fff" }}>
            {view === "table"
              ? this.renderDatedShiftsTable()
              : this.renderDatedShifts()}
          </Content>
        </>
      );
    } else {
      return (
        <>
          <Header style={{ background: "#fff", padding: `0 24px` }}>
            <Row type="flex">
              <Col span={20}>Feed</Col>
            </Row>
          </Header>
          <Content style={{ padding: 24, background: "#fff" }}>
            <Icon type="loading" />
          </Content>
        </>
      );
    }
  }
}

const mapStateToProps = state => ({ ...state });

export default connect(
  mapStateToProps,
  null
)(FeedRoute);
