import React from "react";
import { connect } from "react-redux";
import moment from "moment";

import {
  Badge,
  Button,
  Drawer,
  Form,
  Popconfirm,
  TimePicker,
  Typography
} from "antd";

import { updateEmployeeShift } from "../../actions";

const ModifyShiftDrawer = props => {
  const {
    closeFunc,
    isOpen,
    data,
    shifts,
    employees,
    roles,
    config: timezone
  } = props;
  const { getFieldDecorator } = props.form;

  const { role_id, employee_id, end_time, start_time } = shifts.shiftsList.find(
      shift => shift.id === data
    ),
    findEmployeeRole = roles.find(role => role.id === role_id),
    findEmployeeDetails = employees.find(
      employee => employee.id === employee_id
    );

  const { first_name, last_name } = findEmployeeDetails;
  const { background_colour } = findEmployeeRole;

  const timeFormat = "h:mm A",
    startMoment = moment.tz(start_time, timezone.timezone),
    endMoment = moment.tz(end_time, timezone.timezone);

  const config = {
    rules: [{ type: "object", required: true, message: "Please select time!" }]
  };

  const handleSubmit = () => {
    props.form.validateFields((err, { start_time, end_time }) => {
      if (err) return;

      props.updateEmployeeShift(
        data,
        start_time.format(),
        end_time.format(),
        3600,
        timezone.timezone
      );

      closeFunc();
    });
  };

  return (
    <Drawer
      title={
        <Typography.Paragraph style={{ marginBottom: 0 }}>
          <Badge color={background_colour} />
          Edit
          <b>{` ${last_name}, ${first_name} `}</b>
          Shift
        </Typography.Paragraph>
      }
      visible={isOpen}
      width={420}
      closable={false}
      onClose={closeFunc}
    >
      <Form layout="inline">
        <Form.Item label="Start time">
          {getFieldDecorator(
            "start_time",
            Object.assign(config, {
              initialValue: startMoment
            })
          )(<TimePicker use12Hours allowClear={false} format={timeFormat} />)}
        </Form.Item>
        <Form.Item label="End time">
          {getFieldDecorator(
            "end_time",
            Object.assign(config, {
              initialValue: endMoment
            })
          )(<TimePicker use12Hours allowClear={false} format={timeFormat} />)}
        </Form.Item>

        <div
          style={{
            position: "absolute",
            left: 0,
            bottom: 0,
            width: "100%",
            borderTop: "1px solid #e9e9e9",
            padding: "10px 16px",
            background: "#fff",
            textAlign: "right"
          }}
        >
          <Button onClick={closeFunc} style={{ marginRight: 8 }}>
            Cancel
          </Button>

          <Popconfirm
            placement="topRight"
            title={`Are you want to update this employee's shift?`}
            okType="primary"
            okText="Yes"
            onConfirm={handleSubmit}
            cancelText="No"
          >
            <Button type="primary">Confirm</Button>
          </Popconfirm>
        </div>
      </Form>
    </Drawer>
  );
};

const WrappedRelatedForm = Form.create({ name: "" })(ModifyShiftDrawer);

const mapStateToProps = state => ({ ...state });

const mapDispatchProps = dispatch => ({
  updateEmployeeShift: (id, start, end, duration, tz) =>
    dispatch(updateEmployeeShift(id, start, end, duration, tz))
});

export default connect(
  mapStateToProps,
  mapDispatchProps
)(WrappedRelatedForm);
